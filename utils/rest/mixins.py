from rest_framework import status
from rest_framework.response import Response


class CustomRestMixin:
    response_serializer_class = None

    def get_response_serializer(self, instance):
        if self.response_serializer_class:
            return self.response_serializer_class(instance=instance)
        return self.serializer_class(instance=instance)


class CustomCreateModelMixin(CustomRestMixin):
    """
    Create a model instance.
    """

    def perform_create(self, serializer):
        return serializer.save()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        response_serializer = self.get_response_serializer(instance)
        return Response(response_serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class CustomUpdateModelMixin(CustomRestMixin):
    """
    Update a model instance.
    """
    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        response_serializer = self.get_response_serializer(instance)
        return Response(response_serializer.data)

    def perform_update(self, serializer):
        return serializer.save()

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)
