from django.urls import path, include
from account.views.user import CreateUser, DeleteUser, ChangeUserFullname, SearchUsers, UserTasks

urlpatterns = [
    path('create/', CreateUser.as_view()),
    path('search/', SearchUsers.as_view()),
    path('<int:pk>/change-fullname/', ChangeUserFullname.as_view()),
    path('<int:pk>/delete/', DeleteUser.as_view()),
    path('<int:pk>/tasks/', UserTasks.as_view()),
]

