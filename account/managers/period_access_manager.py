from django.core.cache import cache
from django.utils.crypto import get_random_string


class PeriodAccessManager:
    class ActionKeys:
        CHANGE_USER_FULLNAME = 'change_user_fullname'
        DELETE_USER = 'delete_user'
        CHANGE_TASK_TITLE = 'change_task_title'
        DELETE_TASK = 'delete_task'

        # 0 - Timeout, 1 - Count in timeout
        __SETTINGS__ = {
            CHANGE_USER_FULLNAME: (60, 5),
            DELETE_USER: (60, 5),
            CHANGE_TASK_TITLE: (60, 5),
            DELETE_TASK: (60, 5),
        }

    @classmethod
    def __get_key__(cls, user, action, obj):
        return '{user_id}-{action}-{obj_id}-{rand}'.format(user_id=user.id, action=action, obj_id=obj.id, rand=get_random_string(12))

    @classmethod
    def __get_key_mask__(cls, user, action):
        return '{user_id}-{action}-*'.format(user_id=user.id, action=action)

    @classmethod
    def set_action(cls, user, action, obj):
        assert action in cls.ActionKeys.__SETTINGS__.keys(), 'Действие {0} не определено'.format(action)
        cache.set(cls.__get_key__(user, action, obj), True, timeout=cls.ActionKeys.__SETTINGS__[action][0])

    @classmethod
    def check_action(cls, user, action):
        assert user.is_authenticated, 'Пользователь не авторизован'
        assert action in cls.ActionKeys.__SETTINGS__.keys(), 'Действие {0} не определено'.format(action)
        if len(cache.keys(cls.__get_key_mask__(user, action))) < cls.ActionKeys.__SETTINGS__[action][1]:
            return True
        return False
