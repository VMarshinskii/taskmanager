from django.contrib import admin

from account.models import User, TemporaryAccessToken

admin.site.register(User)
admin.site.register(TemporaryAccessToken)