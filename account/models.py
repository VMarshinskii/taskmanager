import binascii
import os

from django.conf import settings
from django.db import models
from django.contrib.auth.models import AbstractUser, PermissionsMixin, Permission
from django.utils import timezone
from django.utils.translation import gettext_lazy as _


class User(AbstractUser):
    first_name = models.CharField(_('first name'), max_length=150)
    middle_name = models.CharField(_('middle name'), max_length=150)
    last_name = models.CharField(_('last name'), max_length=150)


class TemporaryAccessToken(models.Model):
    key = models.CharField(verbose_name='Ключ', max_length=40, primary_key=True, blank=True)
    user = models.OneToOneField(settings.AUTH_USER_MODEL, verbose_name='Пользователь', on_delete=models.CASCADE)
    permissions = models.ManyToManyField(Permission, verbose_name='Права доступа')
    created_datetime = models.DateTimeField(verbose_name='Дата и время создания', auto_now_add=True)
    start_datetime = models.DateTimeField(verbose_name='Дата и время старта')
    end_datetime = models.DateTimeField(verbose_name='Дата и время окончания')

    class Meta:
        verbose_name_plural = 'Временные токены доступа'
        verbose_name = 'Временный токен доступа'

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        return super().save(*args, **kwargs)

    def generate_key(self):
        return binascii.hexlify(os.urandom(20)).decode()

    def __str__(self):
        return self.key

    @classmethod
    def get_token(cls, key):
        try:
            return cls.objects.get(key=key)
        except cls.DoesNotExist:
            return None

    @classmethod
    def check_permission(cls, user, token_key, model, permission_key):
        token = cls.get_token(token_key)
        if token and token.start_datetime <= timezone.now() <= token.end_datetime and \
                token.permissions.filter(content_type__model=model, codename=permission_key).exists():
            return True
        return False
