from django.db.models import Q
from rest_framework import permissions
from rest_framework.generics import CreateAPIView, get_object_or_404, DestroyAPIView, UpdateAPIView, ListAPIView
from rest_framework.permissions import IsAuthenticated

from account.managers.period_access_manager import PeriodAccessManager
from account.models import User
from account.serializers.user import CreateUserSerializer, ChangeUserFullnameSerializer, \
    UserSerializer
from tasks.models import Task
from tasks.serializers.task import TaskSerializer
from utils.rest.mixins import CustomCreateModelMixin, CustomUpdateModelMixin


class IsChangeUserFullnamePermission(permissions.BasePermission):
    message = 'Превышено колчество изменений ФИО в период'

    def has_permission(self, request, obj):
        return True

    def has_object_permission(self, request, view, obj):
        return PeriodAccessManager.check_action(request.user, PeriodAccessManager.ActionKeys.CHANGE_USER_FULLNAME)


class IsDeletePermission(permissions.BasePermission):
    message = 'Превышено колчество удалений в период'

    def has_permission(self, request, obj):
        return True

    def has_object_permission(self, request, view, obj):
        return PeriodAccessManager.check_action(request.user, PeriodAccessManager.ActionKeys.DELETE_USER)


class CreateUser(CustomCreateModelMixin, CreateAPIView):
    """
    Создание пользователя
    """
    serializer_class = CreateUserSerializer
    response_serializer_class = UserSerializer


class ChangeUserFullname(CustomUpdateModelMixin, UpdateAPIView):
    """
    Изменение ФИО пользователя
    """
    serializer_class = ChangeUserFullnameSerializer
    response_serializer_class = UserSerializer
    permission_classes = (IsAuthenticated, IsChangeUserFullnamePermission)

    def get_object(self):
        obj = get_object_or_404(User, id=self.kwargs.get('pk'))
        self.check_object_permissions(self.request, obj)
        return obj

    def perform_update(self, serializer):
        instance = serializer.save()
        PeriodAccessManager.set_action(self.request.user, PeriodAccessManager.ActionKeys.CHANGE_USER_FULLNAME, instance)


class SearchUsers(ListAPIView):
    """
    Поиск пользователей по ФИО
    """
    serializer_class = UserSerializer

    def get_queryset(self):
        query = self.request.GET.get('q')
        if query:
            q = Q(first_name__icontains=query) | Q(middle_name__icontains=query) | Q(last_name__icontains=query)
            return User.objects.filter(q)
        return User.objects.all()


class DeleteUser(DestroyAPIView):
    """
    Удаление пользователя
    """
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated, IsDeletePermission, )
    model = User

    def get_object(self):
        obj = get_object_or_404(User, id=self.kwargs.get('pk'))
        self.check_object_permissions(self.request, obj)
        return obj

    def perform_destroy(self, instance):
        PeriodAccessManager.set_action(self.request.user, PeriodAccessManager.ActionKeys.DELETE_USER, instance)
        instance.delete()


class UserTasks(ListAPIView):
    """
    Список задач пользователя
    """
    serializer_class = TaskSerializer

    def get_queryset(self):
        return Task.objects.filter(users=self.request.user)
