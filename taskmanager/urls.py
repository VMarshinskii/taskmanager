from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from django.views.generic import RedirectView
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='Pastebin API')

urlpatterns = [
    url(r'^$', RedirectView.as_view(url='/docs/', permanent=True)),
    path('admin/', admin.site.urls),
    path('api-token-auth/', obtain_auth_token),
    path('api/users/', include('account.urls.user')),
    path('api/tasks/', include('tasks.urls.task')),
    path('docs/', schema_view)
]

