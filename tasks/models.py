from django.conf import settings
from django.db import models


class Task(models.Model):
    title = models.CharField(verbose_name='Название', max_length=200)
    description = models.TextField(verbose_name='Описание', default='', blank=True)

    creator = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name='Создатель', on_delete=models.CASCADE)
    users = models.ManyToManyField(settings.AUTH_USER_MODEL, verbose_name='Пользователи', related_name='task_members')

    datetime_create = models.DateTimeField(verbose_name='Дата и время создания', auto_now_add=True)
    datetime_update = models.DateTimeField(verbose_name='Дата и время обновления', auto_now_add=True)

    class Meta:
        verbose_name_plural = 'Задачи'
        verbose_name = 'Задача'
        permissions = [
            ("change_executor", "Может назначать/снимать исполнителя"),
        ]

    def __str__(self):
        return self.title

    def add_executor(self, user):
        if user not in self.users.all():
            self.users.add(user)
        return True

    def remove_executor(self, user):
        if user in self.users.all():
            self.users.remove(user)
        return True
