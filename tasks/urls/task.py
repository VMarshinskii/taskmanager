from django.urls import path
from tasks.views.task import CreateTask, ChangeTaskTitle, SearchTasks, DeleteTask, AddExecutor, RemoveExecutor, \
    TaskExecutors

urlpatterns = [
    path('create/', CreateTask.as_view()),
    path('search/', SearchTasks.as_view()),
    path('<int:pk>/change-task-title/', ChangeTaskTitle.as_view()),
    path('<int:pk>/delete/', DeleteTask.as_view()),
    path('<int:pk>/add-executor/', AddExecutor.as_view()),
    path('<int:pk>/remove-executor/', RemoveExecutor.as_view()),
    path('<int:pk>/executors/', TaskExecutors.as_view()),
]

