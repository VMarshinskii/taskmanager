from rest_framework import serializers

from account.models import User
from tasks.models import Task


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = '__all__'


class CreateTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        exclude = ('creator', )


class ChangeTaskTitleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ('title', )


class ChangeExecutorSerializer(serializers.Serializer):
    user = serializers.IntegerField()
    token = serializers.CharField()

    def validate(self, data):
        try:
            user = User.objects.get(id=data.get('user'))
        except User.DoesNotExist:
            raise serializers.ValidationError({'user': 'Пользователь с id {0} не найден'.format(data.get('user'))})
        return {'user': user}