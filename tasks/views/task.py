from rest_framework import permissions, status
from rest_framework.generics import CreateAPIView, get_object_or_404, DestroyAPIView, UpdateAPIView, ListAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from account.managers.period_access_manager import PeriodAccessManager
from account.models import TemporaryAccessToken
from account.serializers.user import UserSerializer
from tasks.models import Task
from tasks.serializers.task import TaskSerializer, CreateTaskSerializer, ChangeTaskTitleSerializer, \
    ChangeExecutorSerializer
from utils.rest.mixins import CustomCreateModelMixin, CustomUpdateModelMixin


class IsChangeTitlePermission(permissions.BasePermission):
    message = 'Превышено колчество изменений названия в период'

    def has_permission(self, request, obj):
        return True

    def has_object_permission(self, request, view, obj):
        return PeriodAccessManager.check_action(request.user, PeriodAccessManager.ActionKeys.CHANGE_TASK_TITLE)


class IsDeletePermission(permissions.BasePermission):
    message = 'Превышено колчество удалений в период'

    def has_permission(self, request, obj):
        return True

    def has_object_permission(self, request, view, obj):
        return PeriodAccessManager.check_action(request.user, PeriodAccessManager.ActionKeys.DELETE_TASK)


class CreateTask(CustomCreateModelMixin, CreateAPIView):
    """
    Создание задачи
    """
    serializer_class = CreateTaskSerializer
    response_serializer_class = TaskSerializer

    def perform_create(self, serializer):
        return serializer.save(creator=self.request.user)


class ChangeTaskTitle(CustomUpdateModelMixin, UpdateAPIView):
    """
    Изменение названия задачи
    """
    serializer_class = ChangeTaskTitleSerializer
    response_serializer_class = TaskSerializer
    permission_classes = (IsAuthenticated, IsChangeTitlePermission)

    def get_object(self):
        obj = get_object_or_404(Task, id=self.kwargs.get('pk'))
        self.check_object_permissions(self.request, obj)
        return obj

    def perform_update(self, serializer):
        instance = serializer.save()
        PeriodAccessManager.set_action(self.request.user, PeriodAccessManager.ActionKeys.CHANGE_TASK_TITLE, instance)


class SearchTasks(ListAPIView):
    """
    Поиск задач
    """
    serializer_class = TaskSerializer

    def get_queryset(self):
        query = self.request.GET.get('q')
        if query:
            return Task.objects.filter(title__icontains=query)
        return Task.objects.all()


class DeleteTask(DestroyAPIView):
    """
    Удаление задачи
    """
    serializer_class = TaskSerializer
    permission_classes = (IsAuthenticated, IsDeletePermission)

    def get_object(self):
        obj = get_object_or_404(Task, id=self.kwargs.get('pk'))
        self.check_object_permissions(self.request, obj)
        return obj

    def perform_destroy(self, instance):
        PeriodAccessManager.set_action(self.request.user, PeriodAccessManager.ActionKeys.DELETE_TASK, instance)
        instance.delete()


class ChangeExecutorMixin:
    serializer_class = ChangeExecutorSerializer
    model = Task

    def change_task_executor(self, task, user):
        pass

    def get_object(self):
        obj = get_object_or_404(Task, id=self.kwargs.get('pk'))
        self.check_object_permissions(self.request, obj)
        return obj

    def put(self, request, *args, **kwargs):
        temporary_token = request.data.get('token')
        if temporary_token:
            if TemporaryAccessToken.check_permission(request.user, temporary_token, 'task', 'change_executor'):
                task = get_object_or_404(Task, id=self.kwargs.get('pk'))
                serializer = self.serializer_class(data=request.data)
                serializer.is_valid(raise_exception=True)
                user = serializer.validated_data.get('user')
                self.change_task_executor(task, user)
                return Response()
            return Response({'detail': 'Временный токен не верный'}, status=status.HTTP_403_FORBIDDEN)
        return Response({'detail': 'Не предоставлен временный токен'}, status=status.HTTP_403_FORBIDDEN)


class AddExecutor(ChangeExecutorMixin, UpdateAPIView):
    """
    Назначить пользователя на задачу
    """
    def change_task_executor(self, task, user):
        return task.add_executor(user)


class RemoveExecutor(ChangeExecutorMixin, UpdateAPIView):
    """
    Снять пользователя с задачи
    """

    def change_task_executor(self, task, user):
        return task.remove_executor(user)


class TaskExecutors(ListAPIView):
    """
    Список назначенных пользователей на задачу
    """
    serializer_class = UserSerializer

    def get_object(self):
        obj = get_object_or_404(Task, id=self.kwargs.get('pk'))
        self.check_object_permissions(self.request, obj)
        return obj

    def get_queryset(self):
        task = self.get_object()
        return task.users.all()